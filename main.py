from timeit import default_timer as timer

sujaya = "" # sujayas fasta

# liest die Fasta ein, ohne Zeilenumbrüche
def read_seq(seq):
    with open("sequence.fasta") as file:
        for line in file.readlines():
            if line.startswith(">"):
                continue
            seq += line.replace("\n", "")
    return seq


def read_rRNA(rRNA):
    with open("gene.txt") as file:
        for line in file.readlines():
            if line.startswith(">"):
                continue
            rRNA += line.replace("\n", "").replace("T", "U")
    return rRNA


# with open("sujaya.fna") as file:
#     for line in file.readlines():
#         if line.startswith(">"):
#             continue
#         sujaya += line.replace("\n", "").replace("T", "U")

# bildet das reverse Komplement eines übergebenen Strings
def reverse_complement(arg_seq):
    comp_bases = {"A":"U", "U":"A", "G":"C", "C":"G"}
    return "".join([comp_bases[base] for base in arg_seq[::-1]])


def match(a, b):
    m = len(a)
    for i in range(m):
        if a[i] != b[i]:
            return False
    return True


def horspool(p, s):
    """"Returns list of tuples containing p at [0] and starting index of a found match at [1]"""
    hits = []
    m = len(p)
    n = len(s)
    shift = dict()
    for i in range(m-1):
        shift[p[i]] = m-i-1
    pos = 0
    while pos < n-m+1:
        if match(p, s[pos:pos+m]):
            hits.append((p, pos))
        last_letter = s[pos+m-1]
        if last_letter in shift:
            pos += shift[last_letter]
        else:
            pos += m
    return hits


def build_substrings(arg_seq):
    """Returns a list of all substrings of given string."""
    substrings = []
    for i in range(len(arg_seq)):
        for j in range(i, -1, -1):
            substrings.append((arg_seq[i-j:i+1], i-j))
    # print(len(substrings))
    # print(sorted(substrings, key=lambda x: len(x[0])))
    return substrings


def find_repeats(arg_seq):
    """Returns list of pairs in tuples with:
     repeat at [0],
     index in original sequence at [1]
     index in reverse sequence at [2]
     index of reverse complement in original sequence at [3]"""
    rev_seq = reverse_complement(arg_seq)
    subs = build_substrings(arg_seq)
    pairs = []
    for substring in subs:
        hits = horspool(substring[0], rev_seq)
        if len(hits) == 0:
            continue
        pairs += [(hit[0], substring[1], hit[1], len(arg_seq) - hit[1] - len(hit[0])) for hit in hits]
    return pairs


def filter_max_repeats(pairs):
    max_repeats = []
    already_added = dict()          # remembers if pair has been added already

    for i in range(len(pairs)):
        repeat = pairs[i]

        if len(repeat[0]) < 4:      # only repeats bigger 3
            continue

        elif abs(repeat[1] - repeat[3]) < len(repeat[0]) + 3:       # no repeats with overlap and minimal distance of 3
            continue

        elif already_added.get((repeat[3], repeat[1]), False):      # don't add pair twice if partner is already added
            continue

        is_max = True

        for j in range(len(pairs)):
            if i == j:          # repeat is always part of itself
                continue
            pair = pairs[j]
            diff = len(pair[0]) - len(repeat[0])

            if diff < 0:        # repeat is bigger than pair; can't be part of smaller repeat
                continue

            elif repeat[1] == pair[1] and repeat[2] == pair[2]:       # repeat is prefix of pair
                is_max = False
                break

            elif repeat[1] == pair[1] + diff and repeat[2] == pair[2] + diff:       # repeat is suffix of pair
                is_max = False
                break

        if is_max:
            max_repeats.append(repeat)
            already_added[(repeat[1], repeat[3])] = True
    return max_repeats


def main():
    rRNA = read_rRNA("")
    seq = read_seq("")
    start = timer()
    repeats = find_repeats(rRNA)             #change "seq" to "rRNA" for 12s rRNA sequence
    max_repeats = filter_max_repeats(repeats)
    end = timer()
    print(end - start)
    # print(len(seq))
    # print(len(find_repeats(seq)))
    # print(find_repeats(seq))
    # print(len(filter_max_repeats(find_repeats(seq))))
    print(sorted(max_repeats, key=lambda x: x[1]))              # sort by index in original sequence
    print(sorted([rep[0] for rep in max_repeats]))
    print(len(max_repeats))
    # for el in max_repeats:
    #     if el[0] == "CAUU":
    #         print(el)
    print(seq)
    # print(seq[::-1])
    print(reverse_complement(seq))


main()
